package com.example.proxumer.utils

import org.junit.Assert.*
import org.junit.Test
import java.util.*

class IntegerUtilsTest {

    @Test
    fun getRandomNumberInRange() {
        val min = 0
        val max = 1
        require(min < max) { "max must be greater than min" }
        val r = Random()
        for (random in 0..10) {
            println(r.nextInt(max - min + 1) + min)
        }
    }

}