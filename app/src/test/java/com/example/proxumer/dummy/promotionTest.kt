package com.example.proxumer.dummy

import com.example.proxumer.utils.IntegerUtils
import org.junit.Test

class promotionTest {

    @Test
    fun getDummyPromotion() {
        val data : MutableList<PromotionModel> = arrayListOf()
        for (index in 0..10) {
            data.add(PromotionModel(
                IntegerUtils.getRandomNumberInRange(25000, 30000),
                IntegerUtils.getRandomNumberInRange(20000, 24999),
                IntegerUtils.getRandomNumberInRange(10, 100),
                "AAAAA",
                PromoType.from(IntegerUtils.getRandomNumberInRange(0, 1)),
                IntegerUtils.getRandomNumberInRange(10, 30)))
        }
        println(data)
    }

    @Test
    fun getPromoTypeByInt() {
        println(PromoType.from(IntegerUtils.getRandomNumberInRange(0, 1)))
    }

}