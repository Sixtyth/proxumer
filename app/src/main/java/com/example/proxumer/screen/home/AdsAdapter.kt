package com.example.proxumer.screen.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.proxumer.R
import com.example.proxumer.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_ads.view.*

class AdsAdapter(private val context: Context) :
    AbstractRecyclerViewAdapter<String, RecyclerView.ViewHolder>() {

    private val options = RequestOptions().apply {
        this.centerCrop()
        this.error(ContextCompat.getDrawable(context, R.drawable.button_disable))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AdsViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_ads,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Glide.with(context)
            .load(items[position])
            .apply(options)
            .into(holder.itemView.imBestOffer)
    }

    internal inner class AdsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}