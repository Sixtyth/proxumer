package com.example.proxumer.screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.proxumer.R
import com.example.proxumer.dummy.Menu.getDummyAchievement
import com.example.proxumer.ui.NewGoalAdapter
import kotlinx.android.synthetic.main.fragment_achievement.*

class AchievementFragment : Fragment() {

    private lateinit var adapter: NewGoalAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_achievement, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvSubTitle.text = getString(R.string.level, 2)

        adapter = NewGoalAdapter(requireActivity(), resources)

        rcvAchievement.layoutManager =
            GridLayoutManager(requireActivity(), 3, GridLayoutManager.VERTICAL, false)

        rcvAchievement.adapter = adapter

        adapter.setList(getDummyAchievement())

    }

    companion object {
        fun newInstance(): AchievementFragment {
            val args = Bundle()
            val fragment = AchievementFragment()
            fragment.arguments = args
            return fragment
        }
    }

}