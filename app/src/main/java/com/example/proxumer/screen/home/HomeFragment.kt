package com.example.proxumer.screen.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proxumer.R
import com.example.proxumer.dummy.Ads
import com.example.proxumer.dummy.Ads.getDummyAds
import com.example.proxumer.dummy.Promotion.getDummyPromotion
import com.example.proxumer.ui.NewGold
import com.example.proxumer.utils.StringUtils
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var adapter: HomeAdapter
    private lateinit var adtBestOffer: AdsAdapter
    private lateinit var adtSuggest: AdsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvGoals.text = getString(R.string.three_goals)
        tvAllSaving.text = StringUtils.formatAllSaving(requireActivity(), R.string.all_saving, 17500)

        adapter = HomeAdapter(requireActivity())
        adtBestOffer = AdsAdapter(requireActivity())
        adtSuggest = AdsAdapter(requireActivity())

        rvPromotion.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvPromotion.adapter = adapter

        rvSuggestForYou.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvSuggestForYou.adapter = adtBestOffer

        rvBastOffer.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvBastOffer.adapter = adtSuggest

        adapter.setList(getDummyPromotion())
        adtBestOffer.setList(getDummyAds())
        adtSuggest.setList(getDummyAds())


        goToNewGold.setOnClickListener {
            startActivity(Intent(activity, NewGold::class.java))
        }

    }

    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

}