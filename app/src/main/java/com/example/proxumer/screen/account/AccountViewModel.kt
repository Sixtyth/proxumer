package com.example.proxumer.screen.account

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.proxumer.BuildConfig
import com.example.proxumer.ui.base.DisposableViewModel
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import org.json.JSONObject
import timber.log.Timber
import java.net.URISyntaxException

class AccountViewModel : DisposableViewModel() {

    private lateinit var hostSocket: Socket
    private lateinit var updateSocket: Socket
    var activity: Activity? = null
    private var isConnected = false
    private var count: Int = 0

    private val mutableStatus = MutableLiveData<Status>()
    val goldStatus: LiveData<Status> = mutableStatus

    private val currentValue: Status
        get() = mutableStatus.value!!

    init {
        mutableStatus.value = Status.Loading(ViewDataBundle())
    }

    private val io = IO.Options().apply {
        this.path = "/update"
    }

    fun openConnectSocket() {
        try {
            hostSocket = IO.socket(BuildConfig.HOST_SOCKET)
        } catch (e: URISyntaxException) {
            Timber.i("$e")
        }
        hostSocket.connect()
        mutableStatus.value = Status.Loading(ViewDataBundle())
    }

    fun openSocketUpdate() {
        try {
            updateSocket = IO.socket(BuildConfig.HOST_UPDATE, io)
        } catch (e: URISyntaxException) {
            Timber.i("$e")
        }
    }

    fun openReceiveEvent() {
        hostSocket.on("new-notification", onNotification)
        hostSocket.on(Socket.EVENT_CONNECT, onConnect)
        hostSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError)

        updateSocket.on(Socket.EVENT_CONNECT, onUpdateConnect)
        updateSocket.on(Socket.EVENT_CONNECT_ERROR, onUpdateConnectError)
    }

    fun disconnectSocket() {
        hostSocket.disconnect()
    }

    fun disconnectSocketUpdate() {
        updateSocket.disconnect()
    }

    fun closeReceiveEvent() {
        hostSocket.off(Socket.EVENT_CONNECT, onConnect)
        hostSocket.off(Socket.EVENT_DISCONNECT, onDisconnect)
        hostSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError)
        hostSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)

        updateSocket.off(Socket.EVENT_CONNECT, onUpdateConnect)
        updateSocket.off(Socket.EVENT_CONNECT_ERROR, onUpdateConnectError)
    }

    private val onConnect = Emitter.Listener {
        activity!!.runOnUiThread {
            if (!isConnected) {
                isConnected = true
            }

            updateSocket.connect()

            mutableStatus.value = Status.Success(ViewDataBundle(event = ConnectStatus.CONNECTED))

            Timber.i("$isConnected")
        }
    }

    private val onDisconnect = Emitter.Listener {
        activity!!.runOnUiThread {
            it.forEach {
                Timber.i("$it")
            }
            isConnected = false
        }
    }

    private val onConnectError = Emitter.Listener {
        isConnected = false
        activity!!.runOnUiThread {
            it.forEach {
                Timber.i("$it")
                mutableStatus.value = Status.Error(ViewDataBundle(event = ConnectStatus.DISCONNECT))
            }
        }
    }

    private val onNotification = Emitter.Listener { args ->
        if (activity == null) {
            return@Listener
        }
        activity!!.runOnUiThread {
            val message = args[0] as JSONObject
            repeat(args.count()) {
                mutableStatus.value = Status.UpdateData(ViewDataBundle(count.toString()))
                count++
                Timber.i("${message.get("message")}")
            }
            mutableStatus.value = Status.Success(ViewDataBundle(event = ConnectStatus.CONNECTED))
        }
    }

    private val onUpdateConnectError = Emitter.Listener {
        isConnected = false
        activity!!.runOnUiThread {
            it.forEach {
                Timber.i("$it")
                mutableStatus.value = Status.Error(ViewDataBundle(event = ConnectStatus.DISCONNECT))
            }
        }
    }


    private val onUpdateConnect = Emitter.Listener {
        activity!!.runOnUiThread {
            Timber.i("$it")
        }
    }

    sealed class Status(val viewDataBundle: ViewDataBundle) {
        class Loading(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Success(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class UpdateData(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Error(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
    }

    data class ViewDataBundle(
        val newNotification: String = "0",
        val event: ConnectStatus = ConnectStatus.UNKNOWN
    )

    enum class ConnectStatus {
        CONNECTED,
        DISCONNECT,
        UNKNOWN
    }

}