package com.example.proxumer.screen

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.proxumer.screen.account.AccountFragment
import com.example.proxumer.screen.home.HomeFragment
import java.lang.IllegalStateException

class ButtonNavigationViewPager(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> HomeFragment.newInstance()
        1 -> BillFragment.newInstance()
        2 -> AchievementFragment.newInstance()
        3 -> AccountFragment.newInstance()
        else -> {
            throw IllegalStateException("can't find the fragment of position")
        }
    }

    override fun getCount(): Int = 4

}