package com.example.proxumer.screen.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.proxumer.R
import com.example.proxumer.dummy.PromoType
import com.example.proxumer.dummy.PromotionModel
import com.example.proxumer.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_gold_promotion.view.*

class HomeAdapter(private val context: Context) :
    AbstractRecyclerViewAdapter<PromotionModel, RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HomeViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_gold_promotion,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.tvDiscountPrice.text =
            context.getString(R.string.format_price, items[position].discountPrice)
        holder.itemView.tvNormalPrice.text =
            context.getString(R.string.format_price, items[position].normalPrice)
        holder.itemView.progressBar.progress = items[position].progressValue
        holder.itemView.tvPromoDes.text = items[position].promoDescription
        holder.itemView.tvReview.text = PromoType.from(items[position].promoType.id).name
        holder.itemView.tvDateLeft.text =
            context.getString(R.string.format_date_left, items[position].dateLeft)

        if (PromoType.from(items[position].promoType.id) == PromoType.GOOD) {
            holder.itemView.imPromoType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_stocks))
            holder.itemView.mainView.setBackgroundResource(R.drawable.border_green_with_radius)
            holder.itemView.tvReview.setTextColor(ContextCompat.getColor(context, R.color.green))
        } else {
            holder.itemView.imPromoType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_travel))
            holder.itemView.mainView.setBackgroundResource(R.drawable.border_sangria_with_radius)
            holder.itemView.tvReview.setTextColor(ContextCompat.getColor(context, R.color.sangria))
        }

    }

    internal inner class HomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}