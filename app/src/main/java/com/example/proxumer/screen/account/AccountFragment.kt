package com.example.proxumer.screen.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.proxumer.R
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.fragment_empty.tvMessage
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class AccountFragment : Fragment() {

    // Lazy injected Presenter instance
    private val viewModel: AccountViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.activity = activity
        viewModel.openConnectSocket()
        viewModel.openSocketUpdate()
        viewModel.openReceiveEvent()
        getRevertDataFromViewModel(viewModel)

    }

    private fun getRevertDataFromViewModel(viewModel: AccountViewModel) {
        viewModel.goldStatus.observe(viewLifecycleOwner, Observer { status ->

            when (status) {

                is AccountViewModel.Status.Loading -> {
                    tvMessage.text = getString(R.string.waiting)
                }

                is AccountViewModel.Status.Success -> {
                    tvMessage.text = getString(R.string.connected)
                }

                is AccountViewModel.Status.UpdateData -> {
                    tvCount.text = status.viewDataBundle.newNotification
                }

                is AccountViewModel.Status.Error -> {
                    tvMessage.text = getString(R.string.loss_connect)
                }

            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.disconnectSocket()
        viewModel.disconnectSocketUpdate()
        viewModel.closeReceiveEvent()
    }

    companion object {
        fun newInstance(): AccountFragment {
            val args = Bundle()
            val fragment = AccountFragment()
            fragment.arguments = args
            return fragment
        }
    }
}