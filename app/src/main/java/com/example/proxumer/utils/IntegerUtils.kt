package com.example.proxumer.utils

import java.util.*

object IntegerUtils {

    fun getRandomNumberInRange(min: Int, max: Int): Int {
        require(min < max) { "max must be greater than min" }
        val r = Random()
        return r.nextInt(max - min + 1) + min
    }

}