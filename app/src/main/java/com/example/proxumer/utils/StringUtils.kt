package com.example.proxumer.utils

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import androidx.annotation.StringRes
import com.example.proxumer.extensions.pixelFromDp

object StringUtils {

    fun formatAllSaving(context: Context, @StringRes id: Int, amount: Int): SpannableString {
        val ss = SpannableString(context.getString(id).format(amount))
        ss.setSpan(
            AbsoluteSizeSpan(context.pixelFromDp(14f).toInt()),
            ss.indexOf(ss.first()),
            11,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            AbsoluteSizeSpan(context.pixelFromDp(14f).toInt()),
            ss.indexOf(" T"),
            ss.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            AbsoluteSizeSpan(context.pixelFromDp(24f).toInt()),
            11,
            ss.indexOf(" T"),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        ss.setSpan(
            StyleSpan(Typeface.BOLD),
            11,
            ss.indexOf(" T"),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return ss
    }

}