package com.example.proxumer.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.proxumer.R
import com.example.proxumer.extensions.hideKeyboard

class EditTextWithPrefix : ConstraintLayout {

    private val tvPrefix: TextView by lazy { rootView.findViewById<TextView>(R.id.tvPrefixName) }
    private val edtAmount: EditText by lazy { rootView.findViewById<EditText>(R.id.edtAmount) }

    private var prefixName = ""
    private var prefixIsBold: Boolean = false
    private var prefixTextDisplay: String = ""
    private var prefixSize: Int = 0
    private var prefixColor: Int = 0
    private var prefixBackground: Int = 0

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        View.inflate(context, R.layout.edit_text_with_prefix, this)
        setUpView()
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.EditTextWithPrefix)
            prefixName = a.getString(R.styleable.EditTextWithPrefix_prefixTextName) ?: ""
            prefixIsBold = a.getBoolean(R.styleable.EditTextWithPrefix_prefixIsBold, false)

            prefixTextDisplay = a.getString(R.styleable.EditTextWithPrefix_prefixTextDisplay) ?: ""

            prefixSize = a.getDimensionPixelSize(R.styleable.EditTextWithPrefix_prefixTextSize, 14)
            prefixColor = a.getColor(
                R.styleable.EditTextWithPrefix_prefixTextNeedColor,
                ContextCompat.getColor(context, R.color.black)
            )
            prefixBackground = a.getResourceId(
                R.styleable.EditTextWithPrefix_prefixBackground,
                R.drawable.border_sangria_with_out_radius
            )

            a.recycle()
        }
    }

    private fun setUpView() {
        tvPrefix.apply {
            this.text = prefixName
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, prefixSize.toFloat())
            this.setTextColor(prefixColor)
            if (prefixIsBold) {
                this.setTypeface(this.typeface, Typeface.BOLD)
            }
        }

        edtAmount.apply {
            this.hint = prefixTextDisplay
            this.hideKeyboard()
        }

        this.rootView.setBackgroundResource(prefixBackground)
    }

    fun getAmount(): String =
        if (edtAmount.text.toString().isBlank() || edtAmount.text.toString().isEmpty()) {
            ""
        } else {
            edtAmount.text.toString()
        }
}