package com.example.proxumer.koin

import com.example.proxumer.screen.account.AccountViewModel
import org.koin.dsl.module

val viewModelModule = module {
    factory { AccountViewModel() }
}