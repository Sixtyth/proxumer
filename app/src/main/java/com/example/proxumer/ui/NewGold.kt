package com.example.proxumer.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.proxumer.R
import com.example.proxumer.dummy.Menu.getDummyMenu
import com.example.proxumer.extensions.hideKeyboard
import kotlinx.android.synthetic.main.activity_new_gold.*

class NewGold : AppCompatActivity() {

    private lateinit var adapter: NewGoalAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_gold)

        adapter = NewGoalAdapter(this@NewGold, resources)

        rvGoalMenu.layoutManager =
            GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)

        rvGoalMenu.adapter = adapter

        adapter.setList(getDummyMenu())

        edtWhatYourGoal.hideKeyboard()

    }

}