package com.example.proxumer.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.proxumer.R
import com.example.proxumer.screen.ButtonNavigationViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nonSwipeableViewPager.adapter =
            ButtonNavigationViewPager(
                supportFragmentManager
            )

        btnNavView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.item_home -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(0, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_bill -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(1, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_achievement -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(2, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_account -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(3, true)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

}
