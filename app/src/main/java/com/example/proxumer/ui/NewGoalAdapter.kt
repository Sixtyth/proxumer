package com.example.proxumer.ui

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.proxumer.R
import com.example.proxumer.dummy.MenuModel
import com.example.proxumer.dummy.MenuType
import com.example.proxumer.ui.base.AbstractRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_goal_menu.view.*
import timber.log.Timber

class NewGoalAdapter(private val context: Context, private val resources: Resources) :
    AbstractRecyclerViewAdapter<MenuModel, RecyclerView.ViewHolder>() {

    private val menu = resources.getStringArray(R.array.goal_mode)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewGoalViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_goal_menu,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        setMenuName(holder.itemView.tvMenuName, items[position].menuType)
        when (items[position].menuType) {
            MenuType.TRAVEL -> setMenuIcon(holder.itemView.imageView, items[position].menuType)
            MenuType.EDUCATION -> setMenuIcon(holder.itemView.imageView, items[position].menuType)
            MenuType.INVEST -> setMenuIcon(holder.itemView.imageView, items[position].menuType)
            MenuType.CLOTHING -> setMenuIcon(holder.itemView.imageView, items[position].menuType)
            MenuType.ACHIEVEMENT -> setMenuIcon(holder.itemView.imageView, items[position].menuType)
        }
        setMenuIcon(holder.itemView.imageView, items[position].menuType)
        setBackgroundColor(holder.itemView.rootView)
    }

    private fun setMenuName(tvMenuName: TextView?, menuType: MenuType) {
        tvMenuName!!.text = if (menuType == MenuType.ACHIEVEMENT) {
            context.getString(R.string.achievement_first_upper)
        } else {
            menu[menuType.id]
        }
    }

    private fun setMenuIcon(imageView: ImageView, menuType: MenuType) {
        imageView.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                R.drawable.ic_suitcase
            )
        )
    }

    private fun setBackgroundColor(rootView: View?) {
        rootView!!.setBackgroundResource(R.drawable.border_sangria_with_out_radius)
    }

    internal inner class NewGoalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}