package com.example.proxumer.extensions

import android.content.Context
import android.util.TypedValue


fun Context.pixelFromDp(dp: Float): Float = dp * this.resources.displayMetrics.density

fun Context.spToPx(context: Context, sp: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_SP,
        sp,
        context.resources.displayMetrics
    )
}