package com.example.proxumer.dummy

import com.example.proxumer.utils.IntegerUtils

object Promotion {

    fun getDummyPromotion(): MutableList<PromotionModel> {
        val data : MutableList<PromotionModel> = arrayListOf()
        for (index in 0..10) {
            data.add(PromotionModel(
                IntegerUtils.getRandomNumberInRange(25000, 30000),
                IntegerUtils.getRandomNumberInRange(20000, 24999),
                IntegerUtils.getRandomNumberInRange(10, 100),
                "AAAAA",
                PromoType.from(IntegerUtils.getRandomNumberInRange(0, 1)),
                IntegerUtils.getRandomNumberInRange(10, 30)))
        }
        return data
    }

}

data class PromotionModel(
    val discountPrice: Int,
    val normalPrice: Int,
    val progressValue: Int,
    val promoDescription: String,
    val promoType: PromoType,
    val dateLeft: Int
)

enum class PromoType(val id: Int) {
    GOOD(0),
    UNHAPPY(1);

    companion object {
        fun from(findValue: Int): PromoType = values().first { it.id == findValue }
    }

}
