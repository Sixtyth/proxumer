package com.example.proxumer.dummy

object Ads {

    fun getDummyAds(): MutableList<String> {
        val data : MutableList<String> = arrayListOf()
        return data.apply {
            data.add("https://previews.123rf.com/images/bryljaev/bryljaev1601/bryljaev160100054/51519864-big-sale-banner-best-offer.jpg")
            data.add("https://www.pngitem.com/pimgs/m/192-1920162_summer-special-badge-with-banner-best-offer-png.png")
            data.add("https://www.pinclipart.com/picdir/big/104-1044270_specials-discounts-and-deals-on-medical-supplies-and.png")
            data.add("https://www.pinclipart.com/picdir/big/104-1044270_specials-discounts-and-deals-on-medical-supplies-and.png")
            data.add("https://cdn0.iconfinder.com/data/icons/golden-labels/512/as466_14-512.png")
        }
    }
}