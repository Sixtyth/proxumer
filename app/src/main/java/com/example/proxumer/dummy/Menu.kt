package com.example.proxumer.dummy

import com.example.proxumer.utils.IntegerUtils

object Menu {

    fun getDummyMenu(): MutableList<MenuModel> {
        val data: MutableList<MenuModel> = arrayListOf()
        data.apply {
            this.add(MenuModel(MenuType.TRAVEL))
            this.add(MenuModel(MenuType.EDUCATION))
            this.add(MenuModel(MenuType.INVEST))
            this.add(MenuModel(MenuType.CLOTHING))
            this.add(MenuModel(MenuType.EDUCATION))
        }
        return data
    }

    fun getDummyAchievement(): MutableList<MenuModel> {
        val data: MutableList<MenuModel> = arrayListOf()
        for (index in 0..20) {
            data.add(
                MenuModel(
                    MenuType.ACHIEVEMENT
                )
            )
        }
        return data
    }

}

data class MenuModel(
    val menuType: MenuType
)

enum class MenuType(val id: Int) {
    TRAVEL(0),
    EDUCATION(1),
    INVEST(2),
    CLOTHING(3),
    ACHIEVEMENT(4);

    companion object {
        fun from(findValue: Int): MenuType = values().first { it.id == findValue }
    }

}